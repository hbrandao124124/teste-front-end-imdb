# DOCUMENTAÇÃO

## Detalhes técnicos

### SOBRE A API DO IMDB:

A [API solicitada do IMDB](https://imdb-api.com) requer (desde Julho de 2023) que seja adquirido um plano pago para utilização, não sendo possível o acesso gratuito aos dados, mesmo criando-se uma conta.
Como solução, foi escolhida a [API do TMDB](https://developer.themoviedb.org/reference/intro/getting-started), que fornece dados similares, e criada uma conta. A chave de autorização se encontra já configurada no arquivo .env do projeto, não sendo necessários outros ajustes.

### Tecnologias

Esta aplicação utiliza:

- Typescript
- React (como solicitado nos requisitos)
- CSS Modules (como solicitado nos requisitos, não utilizando frameworks de CSS ou UI)
- Vite (para execução e bundling)
- Vitest (como framework de testes)
- Redux (para manter o estado do termo de pesquisa entre páginas, como solicitado)
- Yarn (como gerenciador de pacotes)

## Instruções de execução

### Pré-requisitos

NodeJS v18.16.0 (recomenda-se a utilização da ferramenta nvm para gestão das versões, ou instalação manual via instalador)

      nvm install 18.16.0
      nvm use 18.16.0

Yarn

      npm install yarn -g

Com apenas estas ferramentas, já é possível executar este projeto.

### Instalação e execução

Antes de qualquer coisa, execute o comando de instalação dos pacotes:

      yarn

Ou

      yarn install

Após isso, os seguintes comandos principais estarão disponíveis através do próprio yarn, e definidos no arquivo package.json:

### Para executar a aplicação localmente:

    yarn start

### Para executar os testes:

    yarn test

### Para verificar a cobertura de testes:

    yarn test:coverage

## Detalhes de utilização

Basta digitar um termo de pesquisa e a aplicação realizará uma pesquisa na api do TMDB com o termo solicitado, exibindo os resultados.

![Página Inicial](.docs/search.png)

![Pesquisa](.docs/search-results.png)

Ao clicar em um dos resultados, o usuário é redirecionado para a página do filme com mais informações, incluindo a listagem do elenco.

![Página do Filme](.docs/sharknado.png)

Para compartilhar a URL da página de pesquisa ou da página de algum filme, é possível clicar no botão flutuante com o ícone de compartilhar no canto inferior direito em ambas as telas, e a URL atual será copiada para a área de transferência.

![Compartilhar](.docs/share.png)

## Cobertura de testes

A seguir, uma imagem contendo evidências da última execução de testes antes da entrega, disponível na pasta .docs do projeto.

## ![Test Coverage](.docs/test-results.png)

---

# Teste front end

## Sobre o teste

A aplicação utilizará a API do imdb pra permitir:

- A busca de filmes por uma palavra chave
- Os filmes retornados pela API devem ser listados
- Ao clicar em um filme, deve ser navegado para uma página que exibe mais informações do filme

## Funcionalidades

Na tela inicial, deve ser exibido uma barra de busca grande onde será digitado um nome do filme pelo usuário.

Ao faze a busca, deve ser exibido logo a baixo da barra de busca a listagem com os filmes encontrados, exibindo as seguintes informações:

- Nome do filme
- Descrição do filme
- Imagem da capa do filme

Ao clicar em um filme, o usuário deve ser redirecionado para uma página onde deve ser exibido mais informações sobre o filme selecionado:

- Ano de publicação do filme
- Titulo do filme
- Descrição do filme
- O enredo do filme (plot)
- Lista de atores, contendo:
  - Nome do ator
  - Imagem do ator
  - Nome do persoangem que interpretou no filme

Deve ser possível compartilhar a url de listagem do filme assim como a url da página de exibição dos detalhados de um filme. De forma que a url compartilhada exiba exatamente as mesmas informações quando acessada.

## Requisitos básicos

- Crie um fork deste projeto (https://gitlab.com/Pottencial/teste-front-end-imdb/-/forks/new). É preciso estar logado na sua conta Gitlab
- Adicione @Pottencial (Pottencial Seguradora) como membro do seu fork. Você pode fazer isto em https://gitlab.com/`your-user`/tech-test-frontend-todo/settings/members
- Utilzar React
- Realizar testes unitários, preferencialmente com jest e react-testing-library. Mas pode utilizar outros frameworks caso prefira
- A aplicação não deve utilizar libs css e/ou javascript diferentes de:
  - react
  - react-dom
  - react-router
- O projeto deve possuir um README com instruções básicas de como rodar a aplicação

Para testes e setup do projeto pode ser utilizado quais libs quiser.

## Requisitos adicionais

- Aqui nós costumamos utilizar redux, e mesmo que essa aplicação seja pequena, seria bom se fosse utilizado para demonstrar seus conhecimentos.
- As páginas devem ficar responsívas, se ajustando aos diferentes tamanhos de tela de forma que nenhum elemento fique cortado
- Nos preocupamos com a tratativa de caminhos alternativos na experiência do usuário, por tanto, avaliaremos por exemplo o comportamento da aplicação caso a busca não retorne um filme, e caso o id do filme presente na url de detalhes do filme não exista.

## API

Deve ser utilizado a API do imdb. Você pode criar sua conta, e ver como utilizar a API no site da [API do IMDb](https://imdb-api.com/api).

Para o problema descrito, deve ser utilizado as rotas:

- https://imdb-api.com/en/API/SearchMovie: Para consultar filmes por uma palavra chave
- https://imdb-api.com/en/API/Title: Para obter mais informações de um filme
