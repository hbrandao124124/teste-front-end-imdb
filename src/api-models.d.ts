interface MovieSearchResponseItem {
  id: string;
  title: string;
  poster_path?: string;
  overview: string;
}

interface MovieSearchResponse {
  results: MovieSearchResponseItem[];
}

interface MovieDetailsResponseCreditsCast {
  id: string;
  name: string;
  character: string;
  profile_path: string;
}

interface MovieDetailsResponseCredits {
  cast: MovieDetailsResponseCreditsCast[];
}

interface MovieDetailsResponse {
  id: string;
  title: string;
  overview: string;
  poster_path: string;
  release_date: string;

  credits: MovieDetailsResponseCredits;
}
