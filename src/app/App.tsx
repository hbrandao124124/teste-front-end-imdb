import { Provider } from "react-redux";
import axios from "axios";
import Router from "./Router";
import { store } from "../store/store";
import "./App.css";
import Fab from "../shared/components/Fab/Fab";

axios.defaults.baseURL = import.meta.env.VITE_API_URL;

function App() {
  return (
    <>
      <Provider store={store}>
        <Router />
      </Provider>
      <Fab
        onClick={() => {
          void navigator.clipboard.writeText(window.location.toString());
          alert("URL copiada para compartilhar!");
        }}
      >
        <i className="material-icons">share</i>
      </Fab>
    </>
  );
}

export default App;
