import React, { Suspense } from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import routes from "./routes";
import PulseProgressIndicator from "../shared/components/PulseProgressIndicator/PulseProgressIndicator";

const MovieSearch = React.lazy(() => import("../features/search/MovieSearch"));
const MovieDetails = React.lazy(() => import("../features/movie/MovieDetails"));

const Router: React.FC = () => (
  <BrowserRouter>
    <Routes>
      <Route
        path={routes.movie.path}
        element={
          <Suspense fallback={<PulseProgressIndicator />}>
            <MovieDetails />
          </Suspense>
        }
      />
      <Route
        path={routes.search.path}
        element={
          <Suspense fallback={<PulseProgressIndicator />}>
            <MovieSearch />
          </Suspense>
        }
      />
    </Routes>
  </BrowserRouter>
);

export default Router;
