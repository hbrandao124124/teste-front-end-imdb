export default Object.freeze({
  search: {
    path: "/",
  },
  movie: {
    path: "/movie/:id",
  },
  error: {
    path: "error",
  },
});
