import { Navigate, useParams } from "react-router-dom";
import { useMovieDetails } from "../../hooks/useMovies";
import ImageWithPlaceholder from "../../shared/components/ImageWithPlaceholder/ImageWithPlaceholder";
import GeneralInformation from "./components/GeneralInformation/GeneralInformation";
import ActorsList from "./components/ActorsList/ActorsList";
import styles from "./MovieDetails.module.css";
import routes from "../../app/routes";
import PulseProgressIndicator from "../../shared/components/PulseProgressIndicator/PulseProgressIndicator";

const MovieDetails = () => {
  const { id } = useParams();
  const { isLoading, data, hasError } = useMovieDetails(id ?? "");

  if (hasError) return <Navigate to={routes.search.path} />;

  return (
    <>
      <div className={styles["animation-wrapper"]}>
        <div className={styles.container}>
          <div className={styles.header}>
            <div className={styles["header-image-container"]}>
              <ImageWithPlaceholder src={data?.image} alt="movie-image" />
            </div>
            <div className={styles["header-title-container"]}>
              {isLoading ? <PulseProgressIndicator /> : <h1>{data?.title}</h1>}
            </div>
          </div>
          <div className={styles.content}>
            {!!data && (
              <>
                <GeneralInformation movie={data} />
                <ActorsList actors={data.actorList} />
              </>
            )}
          </div>
        </div>
      </div>
    </>
  );
};

export default MovieDetails;
