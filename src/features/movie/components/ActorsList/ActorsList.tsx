import ActorsListItem from "./ActorsListItem";
import styles from "./ActorsList.module.css";

interface ActorsCardProps {
  actors: Actor[];
}

const ActorsList: React.FC<ActorsCardProps> = ({ actors }) => {
  return (
    <section className={styles.section}>
      <h1>Elenco</h1>
      {!actors.length && <h4>Dados de elenco indisponíveis</h4>}
      {!!actors.length && (
        <div className={styles.container}>
          {actors.map((a) => (
            <ActorsListItem key={a.id} actor={a} />
          ))}
        </div>
      )}
    </section>
  );
};

export default ActorsList;
