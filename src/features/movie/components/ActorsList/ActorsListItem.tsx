import ImageWithPlaceholder from "../../../../shared/components/ImageWithPlaceholder/ImageWithPlaceholder";
import styles from "./ActorsListItem.module.css";

interface ActorsListItemProps {
  actor: Actor;
}

const ActorsListItem: React.FC<ActorsListItemProps> = ({ actor }) => {
  return (
    <div className={styles.container}>
      <div className={styles["image-container"]}>
        <ImageWithPlaceholder
          key={actor.id}
          alt={actor.name}
          src={actor.image}
        />
      </div>
      <div className={styles.content}>
        <h4>{actor.name}</h4>
        <p>como {actor.asCharacter}</p>
      </div>
    </div>
  );
};

export default ActorsListItem;
