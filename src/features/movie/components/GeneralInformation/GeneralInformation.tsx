import styles from "./GeneralInformation.module.css";

interface GeneralInformationProps {
  movie: MovieDetails;
}

const GeneralInformation: React.FC<GeneralInformationProps> = ({ movie }) => {
  return (
    <section className={styles.section}>
      <h1>Detalhes do filme</h1>
      <div className={styles.content}>
        <div>
          <h4>Ano de lançamento:</h4>
          <span>{movie.releaseDate?.getFullYear() ?? "Indisponível"}</span>
        </div>
        <div>
          <h4>Enredo:</h4>
          <p>{movie.plot ? movie.plot : "Indisponível"}</p>
        </div>
      </div>
    </section>
  );
};

export default GeneralInformation;
