import { useMemo } from "react";
import { useMoviesSearch } from "../../hooks/useMovies";
import TextField from "../../shared/components/TextField/TextField";
import PulseProgressIndicator from "../../shared/components/PulseProgressIndicator/PulseProgressIndicator";
import SearchResults from "./components/SearchResults";
import styles from "./MovieSearch.module.css";
import useSearchQuery from "./hooks/useSearchQuery";
import strings from "../../shared/constants/strings";

const MovieSearch = () => {
  const { queryText, debouncedQueryText, setQueryText, isTyping } =
    useSearchQuery();

  const { isLoading, data, hasError } = useMoviesSearch(debouncedQueryText);

  const shouldShowResults = data?.length && debouncedQueryText.length;
  const queryHasNoResults =
    queryText.value.length &&
    !data?.length &&
    !hasError &&
    !isLoading &&
    !isTyping;

  const error = useMemo(() => {
    if (hasError) return strings.SEARCH_REQUEST_ERROR;

    if (queryHasNoResults) return strings.SEARCH_NO_RESULTS;

    return undefined;
  }, [hasError, queryHasNoResults]);

  const titleContainerStyles = !shouldShowResults
    ? [styles["title-container"]]
    : [styles["title-container"], styles["hide-title-container"]];

  const resultsContainerStyles = shouldShowResults
    ? [styles["results-container"]]
    : [styles["results-container"], styles["hide-results-container"]];

  return (
    <div className={styles.container}>
      <div className={styles["search-container"]}>
        <div className={titleContainerStyles.join(" ")}>
          <h1>Pesquisador de Filmes</h1>
        </div>
        <TextField
          placeholder="Digite o nome do filme que deseja encontrar..."
          value={queryText.value}
          onChange={(query) => {
            setQueryText(query);
          }}
          endIcon={
            isTyping || isLoading ? <PulseProgressIndicator /> : "search"
          }
          error={error}
        />
      </div>
      <div className={resultsContainerStyles.join(" ")}>
        {!!data?.length && <SearchResults items={data} />}
      </div>
    </div>
  );
};

export default MovieSearch;
