import Card from "../../../shared/components/Card/Card";
import ImageWithPlaceholder from "./../../../shared/components/ImageWithPlaceholder/ImageWithPlaceholder";
import styles from "./SearchResultItem.module.css";

interface SearchResultItemProps {
  movie: MovieSearchResult;
  index: number;
  onClick: (movie: MovieSearchResult) => void;
}

const SearchResultItem: React.FC<SearchResultItemProps> = ({
  movie,
  index,
  onClick,
}) => {
  return (
    <Card
      className={styles.container}
      onClick={() => {
        onClick(movie);
      }}
      data-testid="search-result-item"
      // Using custom CSS properties to add individual delay to the animation of each list item
      // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment, @typescript-eslint/no-explicit-any
      style={{ "--animation-order": index } as any}
    >
      <div className={styles["image-container"]}>
        <ImageWithPlaceholder src={movie.image} alt="movie-image" />
      </div>
      <div className={styles.content}>
        <h2>{movie.title}</h2>
        <div className={styles.description}>
          <p>{movie.description}</p>
        </div>
      </div>
    </Card>
  );
};

export default SearchResultItem;
