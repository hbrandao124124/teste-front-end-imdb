import { generatePath, useNavigate } from "react-router-dom";
import SearchResultItem from "./SearchResultItem";
import routes from "../../../app/routes";
import styles from "./SearchResults.module.css";

const SearchResults: React.FC<{ items: MovieSearchResult[] }> = ({ items }) => {
  const navigate = useNavigate();

  const openMovieDetails = (movie: MovieSearchResult) => {
    navigate(generatePath(routes.movie.path, { id: movie.id }));
  };

  return (
    <div className={styles.container}>
      {items.map((movie, index) => (
        <SearchResultItem
          key={movie.id}
          movie={movie}
          index={index}
          onClick={openMovieDetails}
        />
      ))}
    </div>
  );
};

export default SearchResults;
