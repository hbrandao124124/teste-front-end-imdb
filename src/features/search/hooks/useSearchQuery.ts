import { useMemo, useState } from "react";
import { searchTermUpdated } from "../../../store/searchSlice";
import { useAppSelector, useAppDispatch } from "../../../store/storeHooks";
import debounce from "lodash.debounce";

const useSearchQuery = () => {
  const queryText = useAppSelector((state) => state.search);
  const dispatch = useAppDispatch();

  const [isTyping, setIsTyping] = useState(false);
  const [debouncedQueryText, setDebouncedQueryText] = useState(queryText.value);

  const setDebouncedQuery = (query: string) => {
    setDebouncedQueryText(query);
    setIsTyping(false);
  };

  const debouncedSetSearchQuery = useMemo(
    () => debounce(setDebouncedQuery, 1000),
    []
  );

  const setQueryText = (query: string) => {
    dispatch(searchTermUpdated(query));
    setIsTyping(true);
    debouncedSetSearchQuery(query);
  };

  return {
    queryText,
    setQueryText,
    debouncedQueryText,
    isTyping,
  };
};

export default useSearchQuery;
