import { useQuery } from "./useQuery";
import * as MoviesService from "../infrastructure/moviesService";

export const useMoviesSearch = (searchQuery: string) =>
  useQuery(
    ["search", searchQuery],
    async () => await MoviesService.search(searchQuery),
    {
      getDisabled: () => !searchQuery.length,
    }
  );

export const useMovieDetails = (id: string) =>
  useQuery(["details", id], async () => await MoviesService.getById(id), {
    getDisabled: () => !id,
  });
