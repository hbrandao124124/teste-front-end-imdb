import { useCallback, useEffect, useState } from "react";

export interface UseQueryOptions {
  getDisabled: () => boolean;
}

export function useQuery<TData>(
  key: readonly unknown[],
  query: () => Promise<TData>,
  options?: UseQueryOptions
) {
  const [isLoading, setIsLoading] = useState(false);
  const [hasError, setHasError] = useState(false);
  const [data, setData] = useState<TData | undefined>();

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const queryFunction = useCallback(async () => await query(), key);

  useEffect(() => {
    let isSubscribed = true;

    if (options?.getDisabled()) {
      setData(undefined);
      return;
    }

    setIsLoading(true);

    async function performQuery() {
      try {
        const results = await queryFunction();

        setHasError(false);
        setData(results);
      } catch {
        setData(undefined);
        setHasError(true);
      } finally {
        setIsLoading(false);
      }
    }

    // Allows promises to be correctly disposed after unmounting the component.
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-condition
    if (isSubscribed) void performQuery();

    return () => {
      isSubscribed = false;
    };
    // Allows queries to be spawned for each specific value for caching
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, key);

  return {
    isLoading,
    data,
    hasError,
  };
}
