import axios from "axios";

export async function search(query: string): Promise<unknown> {
  const { data } = await axios.get<unknown>(
    `/search/movie?query=${query}&api_key=${
      import.meta.env.VITE_API_KEY
    }&language=pt-BR`
  );
  return data;
}

export async function getById(id: string): Promise<unknown> {
  const { data } = await axios.get<unknown>(
    `/movie/${id}?append_to_response=credits&api_key=${
      import.meta.env.VITE_API_KEY
    }&language=pt-BR`
  );
  return data;
}
