import { generateImageUrlFromPath } from "../shared/helpers/baseUrl";

export const mapSearchResponseItemToSearchResult = (
  searchItem: MovieSearchResponseItem
): MovieSearchResult => ({
  id: searchItem.id,
  image: generateImageUrlFromPath(searchItem.poster_path),
  title: searchItem.title,
  description: searchItem.overview,
});

export const mapDetailsResponseToDetails = (
  detailsResponse: MovieDetailsResponse
): MovieDetails => ({
  id: detailsResponse.id,
  title: detailsResponse.title,
  plot: detailsResponse.overview,
  releaseDate: new Date(Date.parse(detailsResponse.release_date)),
  image: generateImageUrlFromPath(detailsResponse.poster_path),
  actorList: detailsResponse.credits.cast.map(
    (c): Actor => ({
      id: c.id,
      image: generateImageUrlFromPath(c.profile_path),
      name: c.name,
      asCharacter: c.character,
    })
  ),
});
