import * as MoviesClient from "./moviesClient";
import {
  mapDetailsResponseToDetails,
  mapSearchResponseItemToSearchResult,
} from "./moviesMapper";

export async function search(query: string): Promise<MovieSearchResult[]> {
  const moviesResponse = (await MoviesClient.search(
    query
  )) as MovieSearchResponse;

  return moviesResponse.results.map((r) =>
    mapSearchResponseItemToSearchResult(r)
  );
}

export async function getById(id: string): Promise<MovieDetails> {
  const detailsResponse = (await MoviesClient.getById(
    id
  )) as MovieDetailsResponse;

  return mapDetailsResponseToDetails(detailsResponse);
}
