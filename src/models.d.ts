interface MovieSearchResult {
  id: string;
  image?: string;
  title: string;
  description: string;
}

interface Actor {
  id: string;
  image?: string;
  name: string;
  asCharacter: string;
}

interface MovieDetails {
  id: string;
  title: string;
  releaseDate?: Date;
  image?: string;
  plot?: string;
  actorList: Actor[];
}
