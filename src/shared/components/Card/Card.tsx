import styles from "./Card.module.css";

const Card: React.FC<
  React.PropsWithChildren<
    React.DetailedHTMLProps<
      React.HTMLAttributes<HTMLDivElement>,
      HTMLDivElement
    >
  >
> = ({ className, children, ...props }) => (
  <div className={[styles.card, className].join(" ")} {...props}>
    {children}
  </div>
);
export default Card;
