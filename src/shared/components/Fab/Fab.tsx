import { PropsWithChildren } from "react";
import styles from "./Fab.module.css";

const Fab: React.FC<
  PropsWithChildren<
    React.DetailedHTMLProps<
      React.ButtonHTMLAttributes<HTMLButtonElement>,
      HTMLButtonElement
    >
  >
> = ({ className, children, ...props }) => {
  return (
    <button className={[styles.fab, className].join(" ")} {...props}>
      {children}
    </button>
  );
};

export default Fab;
