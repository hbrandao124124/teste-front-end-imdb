import styles from "./ImageWithPlaceholder.module.css";

interface ImageWithPlaceholderProps {
  src?: string;
  alt?: string;
}

const ImageWithPlaceholder: React.FC<ImageWithPlaceholderProps> = ({
  src,
  alt,
}) => {
  return (
    <div className={styles.container}>
      {src ? (
        <img src={src} alt={alt} />
      ) : (
        <div className={styles["placeholder-container"]}>
          <h4>IMAGEM INDISPONÍVEL</h4>
        </div>
      )}
    </div>
  );
};

export default ImageWithPlaceholder;
