import styles from "./PulseProgressIndicator.module.css";

const PulseProgressIndicator = () => {
  return <div data-testid="pulse-progress-indicator" className={styles["dot-pulse"]}></div>;
};

export default PulseProgressIndicator;
