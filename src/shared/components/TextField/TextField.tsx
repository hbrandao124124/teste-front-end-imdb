import styles from "./TextField.module.css";

interface TextFieldProps {
  label?: string;
  placeholder: string;
  value: string;
  onChange: (newValue: string) => void;
  endIcon?: React.ReactNode;
  error?: string | boolean;
}

const TextField: React.FC<TextFieldProps> = ({
  placeholder,
  value,
  onChange,
  endIcon,
  error,
}) => {
  return (
    <div className={styles.wrapper}>
      <div className={styles.container}>
        <input
          id="textfield"
          type="search"
          width="100%"
          className={styles.input}
          placeholder={placeholder}
          value={value}
          onChange={(e) => {
            onChange(e.target.value);
          }}
        />
        <div className={styles["end-adornment"]}>
          {typeof endIcon === "string" ? (
            <i className="material-icons">{endIcon}</i>
          ) : (
            endIcon
          )}
        </div>
      </div>
      {error && <span className={styles["error-message"]}>{error}</span>}
    </div>
  );
};

export default TextField;
