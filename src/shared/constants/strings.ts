const strings = {
  SEARCH_REQUEST_ERROR:
    "Não foi possível realizar a pesquisa. Tente novamente mais tarde.",
  SEARCH_NO_RESULTS:
    "Sua pesquisa não retornou nenhum resultado. Tente usar outros termos! :)",
};

export default strings;
