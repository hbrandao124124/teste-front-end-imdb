export const generateImageUrlFromPath = (path?: string) =>
  path ? `${import.meta.env.VITE_IMAGES_BASE_URL}/${path}` : undefined;
