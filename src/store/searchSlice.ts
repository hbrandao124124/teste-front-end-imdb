import { createSlice } from "@reduxjs/toolkit";

interface SearchQueryState {
  value: string;
}

const initialState: SearchQueryState = {
  value: "",
};

const searchSlice = createSlice({
  name: "search",
  initialState,
  reducers: {
    searchTermUpdated(state, action) {
      const value = action.payload as string;
      state.value = value;
    },
  },
});

export const { searchTermUpdated } = searchSlice.actions;
export default searchSlice.reducer;
