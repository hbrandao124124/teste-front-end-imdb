import {
  mapDetailsResponseToDetails,
  mapSearchResponseItemToSearchResult,
} from "../../src/infrastructure/moviesMapper";

export const mockedMovieDetailsResponse: MovieDetailsResponse = {
  id: "1",
  credits: {
    cast: [
      {
        id: "1",
        character: "MovieDetailsCharacter1",
        name: "MovieDetailsName1",
        profile_path: "MovieDetailsProfilePath1",
      },
      {
        id: "2",
        character: "MovieDetailsCharacter2",
        name: "MovieDetailsName2",
        profile_path: "MovieDetailsProfilePath3",
      },
      {
        id: "3",
        character: "MovieDetailsCharacter3",
        name: "MovieDetailsName3",
        profile_path: "MovieDetailsProfilePath3",
      },
    ],
  },
  overview: "MovieDetailsOverview",
  poster_path: "MovieDetailsPosterPath",
  release_date: "2023-05-01",
  title: "MovieDetailsTitle",
};

export const mockedMoviesSearchResponse: MovieSearchResponse = {
  results: [
    {
      id: "1",
      title: "TestMovie1",
      poster_path: undefined,
      overview: "TestMovie1Overview",
    },
    {
      id: "2",
      title: "TestMovie2",
      poster_path: undefined,
      overview: "TestMovie2Overview",
    },
    {
      id: "3",
      title: "TestMovie3",
      poster_path: undefined,
      overview: "TestMovie3Overview",
    },
  ],
};

export const mockedMoviesSearchResult: MovieSearchResult[] =
  mockedMoviesSearchResponse.results.map((r) =>
    mapSearchResponseItemToSearchResult(r)
  );

export const mockedMovieDetails: MovieDetails = mapDetailsResponseToDetails(
  mockedMovieDetailsResponse
);
