import { render, screen } from "@testing-library/react";
import { Navigate, useParams } from "react-router-dom";
import { useMovieDetails } from "../../../src/hooks/useMovies";
import { mockedMovieDetails } from "../../data/movies";
import GeneralInformation from "../../../src/features/movie/components/GeneralInformation/GeneralInformation";
import ActorsList from "../../../src/features/movie/components/ActorsList/ActorsList";
import ImageWithPlaceholder from "../../../src/shared/components/ImageWithPlaceholder/ImageWithPlaceholder";
import MovieDetails from "../../../src/features/movie/MovieDetails";

vi.mock("../../../src/hooks/useMovies");
vi.mock("react-router-dom");
vi.mock(
  "../../../src/features/movie/components/GeneralInformation/GeneralInformation"
);
vi.mock("../../../src/features/movie/components/ActorsList/ActorsList");
vi.mock(
  "../../../src/shared/components/ImageWithPlaceholder/ImageWithPlaceholder"
);

describe("<MovieDetails />", () => {
  beforeEach(() => {
    vi.resetAllMocks();
    vi.mocked(useParams).mockImplementation(() => ({ id: "id" }));
  });

  it("should cover undefined id", () => {
    vi.mocked(useParams).mockImplementation(() => ({ id: undefined }));
    vi.mocked(useMovieDetails).mockImplementation(() => ({
      hasError: true,
      isLoading: false,
      data: undefined,
    }));

    render(<MovieDetails />);

    expect(Navigate).toHaveBeenCalled();
  });

  it("redirects to search on error", () => {
    vi.mocked(useMovieDetails).mockImplementation(() => ({
      hasError: true,
      isLoading: false,
      data: undefined,
    }));

    render(<MovieDetails />);

    expect(Navigate).toHaveBeenCalled();
  });

  it("should show loading in place of title when loading", () => {
    vi.mocked(useMovieDetails).mockImplementation(() => ({
      hasError: false,
      isLoading: true,
      data: undefined,
    }));

    render(<MovieDetails />);

    expect(screen.getByTestId("pulse-progress-indicator")).toBeInTheDocument();
  });

  it("should show data with successful query", () => {
    vi.mocked(useMovieDetails).mockImplementation(() => ({
      hasError: false,
      isLoading: false,
      data: mockedMovieDetails,
    }));

    render(<MovieDetails />);

    expect(
      screen.queryByText("pulse-progress-indicator")
    ).not.toBeInTheDocument();

    expect(ImageWithPlaceholder).toHaveBeenCalledOnce();
    expect(screen.getByText(mockedMovieDetails.title)).toBeInTheDocument();
    expect(GeneralInformation).toHaveBeenCalledWith(
      {
        movie: mockedMovieDetails,
      },
      {}
    );
    expect(ActorsList).toHaveBeenCalledWith(
      {
        actors: mockedMovieDetails.actorList,
      },
      {}
    );
  });
});
