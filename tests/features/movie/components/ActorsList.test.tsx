import { render, screen } from "@testing-library/react";
import ActorsListItem from "../../../../src/features/movie/components/ActorsList/ActorsListItem";
import ActorsList from "../../../../src/features/movie/components/ActorsList/ActorsList";
import { mockedMovieDetails } from "../../../data/movies";

vi.mock("../../../../src/features/movie/components/ActorsList/ActorsListItem");

describe("<ActorsList />", () => {
  it("should render without actors", () => {
    render(<ActorsList actors={[]} />);

    expect(screen.getByText("Elenco")).toBeInTheDocument();
    expect(
      screen.getByText("Dados de elenco indisponíveis")
    ).toBeInTheDocument();
    expect(ActorsListItem).not.toHaveBeenCalled();
  });

  it("should render with actors", () => {
    render(<ActorsList actors={mockedMovieDetails.actorList} />);

    expect(screen.getByText("Elenco")).toBeInTheDocument();
    expect(
      screen.queryByText("Dados de elenco indisponíveis")
    ).not.toBeInTheDocument();
    expect(ActorsListItem).toHaveBeenCalledTimes(
      mockedMovieDetails.actorList.length
    );
  });
});
