import { render, screen } from "@testing-library/react";
import ActorsListItem from "../../../../src/features/movie/components/ActorsList/ActorsListItem";
import ImageWithPlaceholder from "../../../../src/shared/components/ImageWithPlaceholder/ImageWithPlaceholder";
import { mockedMovieDetails } from "../../../data/movies";

vi.mock(
  "../../../../src/shared/components/ImageWithPlaceholder/ImageWithPlaceholder"
);

describe("<ActorListItem />", () => {
  it("should render with actor data", () => {
    const actorData = mockedMovieDetails.actorList[0];

    render(<ActorsListItem actor={actorData} />);

    expect(ImageWithPlaceholder).toHaveBeenCalledWith(
      { src: actorData.image, alt: actorData.name },
      {}
    );
    expect(screen.getByText(actorData.name)).toBeInTheDocument();
    expect(
      screen.getByText(actorData.asCharacter, { exact: false })
    ).toBeInTheDocument();
  });
});
