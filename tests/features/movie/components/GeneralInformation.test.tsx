/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { render, screen } from "@testing-library/react";
import { mockedMovieDetails } from "../../../data/movies";
import GeneralInformation from "../../../../src/features/movie/components/GeneralInformation/GeneralInformation";

vi.mock(
  "../../../../src/shared/components/ImageWithPlaceholder/ImageWithPlaceholder"
);

describe("<GeneralInformation />", () => {
  it.each([false, true])("should render with data available", (hasData) => {
    const movie = {
      ...mockedMovieDetails,
      plot: hasData ? mockedMovieDetails.plot : undefined,
      releaseDate: hasData ? mockedMovieDetails.releaseDate : undefined,
    };

    render(<GeneralInformation movie={movie} />);

    if (!hasData) {
      expect(screen.getAllByText("Indisponível")).toHaveLength(2);
    } else {
      expect(screen.getByText(movie.releaseDate!.getFullYear())).toBeInTheDocument();
      expect(screen.getByText(movie.plot!)).toBeInTheDocument();
    }

    expect(screen.getByText("Detalhes do filme")).toBeInTheDocument();
    expect(screen.getByText("Ano de lançamento:")).toBeInTheDocument();
    expect(screen.getByText("Enredo:")).toBeInTheDocument();
  });
});
