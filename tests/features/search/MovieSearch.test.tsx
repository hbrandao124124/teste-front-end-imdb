import { fireEvent, render, screen } from "@testing-library/react";
import { useMoviesSearch } from "../../../src/hooks/useMovies";
import useSearchQuery from "../../../src/features/search/hooks/useSearchQuery";
import MovieSearch from "../../../src/features/search/MovieSearch";
import SearchResults from "../../../src/features/search/components/SearchResults";
import strings from "../../../src/shared/constants/strings";
import { mockedMoviesSearchResult } from "../../data/movies";

vi.mock("../../../src/features/search/hooks/useSearchQuery");
vi.mock("../../../src/hooks/useMovies");
vi.mock("../../../src/features/search/components/SearchResults");

const SearchInput = () =>
  screen.getByPlaceholderText("Digite o nome do filme que deseja encontrar...");

describe("<MovieSearch />", () => {
  it("should render initial state", () => {
    vi.mocked(useSearchQuery).mockReturnValueOnce({
      queryText: { value: "" },
      debouncedQueryText: "",
      isTyping: false,
      setQueryText: vi.fn(),
    });
    vi.mocked(useMoviesSearch).mockReturnValueOnce({
      isLoading: false,
      data: undefined,
      hasError: false,
    });

    render(<MovieSearch />);

    expect(SearchResults).not.toHaveBeenCalled();
    expect(screen.getByText("Pesquisador de Filmes")).toBeInTheDocument();
    expect(SearchInput()).toBeInTheDocument();
    expect(screen.getByText("search")).toBeInTheDocument();
  });

  it.each([
    [false, strings.SEARCH_NO_RESULTS],
    [true, strings.SEARCH_REQUEST_ERROR],
  ])(
    "should handle request errors",
    (hasError: boolean, errorMessage: string) => {
      const useSearchQueryValue = {
        queryText: { value: "Test" },
        debouncedQueryText: "",
        isTyping: false,
        setQueryText: vi.fn(),
      };

      const useMoviesSearchValue = {
        isLoading: false,
        data: undefined,
        hasError: hasError,
      };

      vi.mocked(useSearchQuery).mockReturnValueOnce(useSearchQueryValue);
      vi.mocked(useMoviesSearch).mockReturnValueOnce(useMoviesSearchValue);

      render(<MovieSearch />);

      expect(SearchResults).not.toHaveBeenCalled();
      expect(screen.getByText("Pesquisador de Filmes")).toBeInTheDocument();
      expect(SearchInput()).toHaveValue(useSearchQueryValue.queryText.value);
      expect(screen.getByText(errorMessage)).toBeInTheDocument();
    }
  );

  it("should set query text on changing search", () => {
    const setQueryTextMock = vi.fn();

    const useSearchQueryValue = {
      queryText: { value: "" },
      debouncedQueryText: "",
      isTyping: false,
      setQueryText: setQueryTextMock,
    };

    const useMoviesSearchValue = {
      isLoading: false,
      data: undefined,
      hasError: false,
    };

    vi.mocked(useSearchQuery).mockReturnValueOnce(useSearchQueryValue);
    vi.mocked(useMoviesSearch).mockReturnValueOnce(useMoviesSearchValue);

    render(<MovieSearch />);

    fireEvent.change(SearchInput(), { target: { value: "Test" } });

    expect(setQueryTextMock).toHaveBeenCalledWith("Test");
  });

  it("should show results when data is available", () => {
    const useSearchQueryValue = {
      queryText: { value: "Query" },
      debouncedQueryText: "Query",
      isTyping: false,
      setQueryText: vi.fn(),
    };

    const useMoviesSearchValue = {
      isLoading: false,
      data: mockedMoviesSearchResult,
      hasError: false,
    };

    vi.mocked(useSearchQuery).mockReturnValueOnce(useSearchQueryValue);
    vi.mocked(useMoviesSearch).mockReturnValueOnce(useMoviesSearchValue);

    render(<MovieSearch />);

    fireEvent.change(SearchInput(), { target: { value: "Test" } });

    expect(SearchResults).toHaveBeenCalled();
  });

  it.each([true, false])(
    "should show loading when typing or loading",
    (useLoading: boolean) => {
      const useSearchQueryValue = {
        queryText: { value: "Query" },
        debouncedQueryText: "Query",
        isTyping: !useLoading,
        setQueryText: vi.fn(),
      };

      const useMoviesSearchValue = {
        isLoading: useLoading,
        data: mockedMoviesSearchResult,
        hasError: false,
      };

      vi.mocked(useSearchQuery).mockReturnValueOnce(useSearchQueryValue);
      vi.mocked(useMoviesSearch).mockReturnValueOnce(useMoviesSearchValue);

      render(<MovieSearch />);

      expect(
        screen.getByTestId("pulse-progress-indicator")
      ).toBeInTheDocument();
    }
  );
});
