import { fireEvent, render, screen } from "@testing-library/react";
import { mockedMoviesSearchResult } from "./../../../data/movies";
import ImageWithPlaceholder from "../../../../src/shared/components/ImageWithPlaceholder/ImageWithPlaceholder";
import SearchResultItem from "../../../../src/features/search/components/SearchResultItem";

vi.mock(
  "../../../../src/shared/components/ImageWithPlaceholder/ImageWithPlaceholder"
);

describe("<SearchResultItem />", () => {
  it("should render movie information", () => {
    const movie = mockedMoviesSearchResult[0];
    const onClick = vi.fn();

    render(<SearchResultItem movie={movie} index={0} onClick={onClick} />);

    expect(ImageWithPlaceholder).toHaveBeenCalled();
    expect(screen.getByText(movie.title)).toBeInTheDocument();
    expect(screen.getByText(movie.description)).toBeInTheDocument();
  });

  it("should call onClick when clicked", () => {
    const movie = mockedMoviesSearchResult[0];
    const onClick = vi.fn();

    render(<SearchResultItem movie={movie} index={0} onClick={onClick} />);

    fireEvent.click(screen.getByTestId("search-result-item"));

    expect(onClick).toHaveBeenCalled();
  });
});
