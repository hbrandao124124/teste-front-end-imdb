import { fireEvent, render, screen } from "@testing-library/react";
import { mockedMoviesSearchResult } from "./../../../data/movies";
import SearchResults from "../../../../src/features/search/components/SearchResults";
import SearchResultItem from "../../../../src/features/search/components/SearchResultItem";
import { useNavigate } from "react-router-dom";

vi.mock("react-router-dom");
vi.mock("../../../../src/features/search/components/SearchResultItem");

describe("<SearchResults />", () => {
  beforeEach(() => {
    vi.resetAllMocks();
  });

  it("should render result items", () => {
    render(<SearchResults items={mockedMoviesSearchResult} />);

    mockedMoviesSearchResult.forEach((r, index) => {
      expect(SearchResultItem).toHaveBeenCalledWith(
        expect.objectContaining({
          movie: r,
          index: index,
        }),
        expect.anything()
      );
    });
  });

  it("should navigate to movie page when movie is clicked", () => {
    const movies = mockedMoviesSearchResult.slice(0, 1);
    const navigate = vi.fn();

    vi.mocked(useNavigate).mockImplementationOnce(() => navigate);
    vi.mocked(SearchResultItem).mockImplementation(({ onClick }) => (
      <div
        onClick={() => {
          onClick(movies[0]);
        }}
      >
        ResultItemMock
      </div>
    ));

    render(<SearchResults items={movies} />);

    fireEvent.click(screen.getByText("ResultItemMock"));

    expect(navigate).toHaveBeenCalled();
  });
});
