import { act, renderHook } from "@testing-library/react";
import {
  useAppDispatch,
  useAppSelector,
} from "../../../../src/store/storeHooks";
import useSearchQuery from "../../../../src/features/search/hooks/useSearchQuery";

vi.mock("../../../../src/store/storeHooks");

describe("useSearchQuery", () => {
  beforeEach(() => {
    vi.resetAllMocks();
  });

  it("should get querytext from state", () => {
    vi.mocked(useAppSelector).mockReturnValue({ value: "Text" });

    const hook = renderHook(() => useSearchQuery());

    expect(hook.result.current.queryText.value).toBe("Text");
  });

  it("should update queryText and debouncedQueryText", async () => {
    vi.useFakeTimers();

    const mockDispatch = vi.fn();
    vi.mocked(useAppSelector).mockReturnValue({ value: "" });
    vi.mocked(useAppDispatch).mockImplementation(() => mockDispatch);

    const hook = renderHook(() => useSearchQuery());

    await act(async () => {
      hook.result.current.setQueryText("Text");
      await Promise.resolve();
    });

    expect(hook.result.current.isTyping).toBeTruthy();
    expect(hook.result.current.debouncedQueryText).toBe("");

    await act(async () => {
      await vi.advanceTimersByTimeAsync(1500);
    });

    expect(hook.result.current.isTyping).toBeFalsy();
    expect(hook.result.current.debouncedQueryText).toBe("Text");
  });
});
