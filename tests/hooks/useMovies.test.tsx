import { UseQueryOptions, useQuery } from "../../src/hooks/useQuery";
import { useMoviesSearch, useMovieDetails } from "../../src/hooks/useMovies";
import { renderHook } from "@testing-library/react";

vi.mock("../../src/hooks/useQuery");
vi.mock("../../src/infrastructure/moviesService");

function mockedUseQueryImplementation<TData>(
  key: readonly unknown[],
  query: () => Promise<TData>,
  options?: UseQueryOptions
) {
  void key;
  void query();
  void options?.getDisabled();
  return { isLoading: false, data: undefined, hasError: false };
}

describe("useMovies", () => {
  beforeEach(() => {
    vi.resetAllMocks();
    vi.mocked(useQuery).mockImplementation(mockedUseQueryImplementation);
  });

  it("useMoviesSearch should return query", () => {
    const useQueryMock = vi.mocked(useQuery);

    renderHook(() => useMoviesSearch(""));

    expect(useQueryMock).toHaveBeenCalledWith(
      expect.arrayContaining(["search", ""]),
      expect.any(Function),
      expect.objectContaining({
        // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
        getDisabled: expect.any(Function),
      })
    );
  });

  it("useMovieDetails should return query", () => {
    const useQueryMock = vi.mocked(useQuery);

    renderHook(() => useMovieDetails(""));

    expect(useQueryMock).toHaveBeenCalledWith(
      expect.arrayContaining(["details", ""]),
      expect.any(Function),
      expect.objectContaining({
        // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
        getDisabled: expect.any(Function),
      })
    );
  });
});
