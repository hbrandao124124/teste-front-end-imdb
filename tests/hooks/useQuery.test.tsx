import { act, renderHook } from "@testing-library/react";
import { useQuery } from "../../src/hooks/useQuery";

const spawnDelayedPromise = (delayMs: number) => {
  return new Promise<[]>((resolve) => {
    setTimeout(() => {
      resolve([]);
    }, delayMs);
  });
};

const mockQuery = vi.fn<[], Promise<unknown>>();

describe("useQuery", () => {
  beforeEach(() => {
    vi.resetAllMocks();
  });

  afterEach(() => {
    vi.useRealTimers();
  });

  it("should have data on successful query", async () => {
    mockQuery.mockResolvedValue([]);

    const hook = renderHook(() => useQuery(["test"], mockQuery));

    await act(async () => {
      hook.rerender();
      await Promise.resolve();
    });

    expect(mockQuery).toHaveBeenCalled();
    expect(hook.result.current.data).toBeDefined();
  });

  it("should have error on query failure", async () => {
    mockQuery.mockRejectedValue([]);

    const hook = renderHook(() => useQuery(["test"], mockQuery));

    await act(async () => {
      hook.rerender();
      await Promise.resolve();
    });

    expect(mockQuery).toHaveBeenCalled();
    expect(hook.result.current.hasError).toBeTruthy();
  });

  it("should be loading on promise not finished", async () => {
    vi.useFakeTimers();
    vi.clearAllTimers();
    mockQuery.mockImplementation(() => spawnDelayedPromise(100));

    const hook = renderHook(() => useQuery(["test"], mockQuery));

    await act(async () => {
      await vi.advanceTimersByTimeAsync(50);
    });

    expect(mockQuery).toHaveBeenCalled();
    expect(hook.result.current.isLoading).toBeTruthy();

    await act(async () => {
      await vi.advanceTimersByTimeAsync(100);
    });

    expect(hook.result.current.isLoading).toBeFalsy();
  });

  it("should not query when disabled", () => {
    mockQuery.mockResolvedValue([]);

    renderHook(() =>
      useQuery(["test"], mockQuery, { getDisabled: () => true })
    );
    expect(mockQuery).not.toHaveBeenCalled();
  });
});
