import axios from "axios";
import * as MoviesClient from "../../src/infrastructure/moviesClient";

vi.mock("axios");

const resultData = {
  value: "Test",
};
const axiosResult = {
  data: resultData,
};

describe("moviesClient", () => {
  beforeEach(() => {
    vi.resetAllMocks();
    // Prevent TS from complaining about axios.get begin used as an unbound method
    // eslint-disable-next-line @typescript-eslint/unbound-method
    vi.mocked(axios.get).mockImplementation(() => Promise.resolve(axiosResult));
  });

  it.each([MoviesClient.search, MoviesClient.getById])(
    "should return result data from calls",
    async (testedFunction: (param: string) => Promise<unknown>) => {
      const result = await testedFunction("");
      expect(result).toBe(resultData);
    }
  );
});
