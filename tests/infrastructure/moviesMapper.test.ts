import {
  mapDetailsResponseToDetails,
  mapSearchResponseItemToSearchResult,
} from "../../src/infrastructure/moviesMapper";
import { generateImageUrlFromPath } from "../../src/shared/helpers/baseUrl";
import {
  mockedMovieDetailsResponse,
  mockedMoviesSearchResponse,
} from "../data/movies";

describe("moviesMapper", () => {
  it.each([undefined, "poster_path"])(
    "should map search response item to movie search result",
    (poster_path?: string) => {
      const searchItemResponse = {
        ...mockedMoviesSearchResponse.results[0],
        poster_path,
      };
      const expected: MovieSearchResult = {
        id: searchItemResponse.id,
        title: searchItemResponse.title,
        image: generateImageUrlFromPath(searchItemResponse.poster_path),
        description: searchItemResponse.overview,
      };

      const result = mapSearchResponseItemToSearchResult(searchItemResponse);

      expect(result).toStrictEqual(expected);
    }
  );

  it.each([undefined, "image_path"])(
    "should map details response to details",
    () => {
      const detailsResponse = mockedMovieDetailsResponse;
      const expected = {
        id: detailsResponse.id,
        title: detailsResponse.title,
        plot: detailsResponse.overview,
        releaseDate: new Date(Date.parse(detailsResponse.release_date)),
        image: generateImageUrlFromPath(detailsResponse.poster_path),
        actorList: detailsResponse.credits.cast.map(
          (c): Actor => ({
            id: c.id,
            image: generateImageUrlFromPath(c.profile_path),
            name: c.name,
            asCharacter: c.character,
          })
        ),
      };

      const result = mapDetailsResponseToDetails(detailsResponse);

      expect(result).toStrictEqual(expected);
    }
  );
});
