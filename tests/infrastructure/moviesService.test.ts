import * as MoviesService from "../../src/infrastructure/moviesService";
import * as MoviesClient from "../../src/infrastructure/moviesClient";
import {
  mockedMovieDetails,
  mockedMovieDetailsResponse,
  mockedMoviesSearchResponse,
  mockedMoviesSearchResult,
} from "../data/movies";

vi.mock("../../src/infrastructure/moviesClient");

describe("moviesService", () => {
  beforeEach(() => {
    vi.resetAllMocks();
  });

  it("should map details", async () => {
    const clientResult = mockedMovieDetailsResponse;
    const expectedResult = mockedMovieDetails;

    vi.mocked(MoviesClient.getById).mockReturnValueOnce(
      Promise.resolve(clientResult)
    );

    const result = await MoviesService.getById("");

    expect(result).toStrictEqual(expectedResult);
  });

  it("should map search results", async () => {
    const clientResult = mockedMoviesSearchResponse;
    const expectedResult = mockedMoviesSearchResult;

    vi.mocked(MoviesClient.search).mockReturnValueOnce(
      Promise.resolve(clientResult)
    );

    const result = await MoviesService.search("");

    expect(result).toStrictEqual(expectedResult);
  });
});
