import { render, screen } from "@testing-library/react";
import ImageWithPlaceholder from "../../../src/shared/components/ImageWithPlaceholder/ImageWithPlaceholder";

describe("<ImageWithPlaceholder />", () => {
  it("should render image when source is available", () => {
    render(<ImageWithPlaceholder src={"test"} alt={"test"} />);
    expect(screen.getByAltText("test")).toBeInTheDocument();
  });

  it("should render placeholder when src is unavailable", () => {
    render(<ImageWithPlaceholder src={undefined} alt={undefined} />);
    expect(screen.getByText("IMAGEM INDISPONÍVEL")).toBeInTheDocument();
  });
});
