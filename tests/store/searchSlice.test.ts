import searchReducer, { searchTermUpdated } from "../../src/store/searchSlice";

describe("searchSlice", () => {
  it("should update search term", () => {
    const previousState = { value: "" };

    const result = searchReducer(previousState, searchTermUpdated("text"));

    expect(result).toEqual({ value: "text" });
  });
});
